/// <reference types="cypress" />

import telaLogon from '../support/frontEnd/pages/logon';
import telaPrincipal from '../support/frontEnd/pages/telaPrincipal';
import finalizarCompras from '../support/frontEnd/pages/finalizarCompra';
//Elelmentos para utilizar: 
//Tela Principal:
const elementosTelaPrincipal = {
    DATATEST_BACKPACK_BUTTON   : '[data-test="add-to-cart-sauce-labs-backpack"]',
    DATATEST_BIKE_LIGTH_BUTTON : '[data-test="add-to-cart-sauce-labs-bike-light"]',
    DATATEST_BOLT_TSHIRT_BUTTON: '[data-test="add-to-cart-sauce-labs-bolt-t-shirt"]',
    DATATEST_JACKET_BUTTON     : '[data-test="add-to-cart-sauce-labs-fleece-jacket"]',
    DATATEST_ONESIE_BUTTON     : '[data-test="add-to-cart-sauce-labs-onesie"]',
    DATATEST_RED_TSHIRT_BUTTON : '[data-test="add-to-cart-test.allthethings()-t-shirt-(red)"]'
}
const elementosLogin = {
    DATATEST_USERNAME_INPUT: '[data-test="username"]',
    DATATEST_PASSWORD_INPUT: '[data-test="password"]',
    DATATEST_LOGIN_BUTTON  : '[data-test="login-button"]'
}
const dadosLogin = {
USUARIO: 'standard_user',
USUARIO_BLOQUEADO: 'locked_out_user',
SENHA: 'secret_sauce'
}
beforeEach(() => {
    //cy.visit('https://www.saucedemo.com/');
    acessarURL();
  })
   
describe('sauce shop', () => {
    context('Contexto do Login', () => { 
        it('Login', () => {
            //cy.visit('https://www.saucedemo.com/');
    
            // cy.get('[data-test="username"]').type("standard_user");
            // cy.get('[data-test="password"]').type("secret_sauce");
            // cy.get('[data-test="login-button"]').click();
            // cy.get('.title').contains("Products");
            
            //Login através de commands 
            cy.login("standard_user","secret_sauce");

            //esta linha entra somente após dar erro no login do exemploo de usuário bloqueado
            cy.get('.title').contains("Products");
            //cy.get(elementosLogin.DATATEST_USERNAME_INPUT).type(informacoesLogin.usuario);
            // cy.get(elementosLogin.DATATEST_PASSWORD_INPUT).type(informacoesLogin.senha);
            // cy.get(elementosLogin.DATATEST_LOGIN_BUTTON).click();
           
            // telaLogon.acessarURL();
            // telaLogon.realizarLogin(objeto.loginUsuarioBloqueado);
            // cy.contains(objeto.loginUsuarioBloqueado.mensagem);
        });
        it('Login Usuário Bloqueado', () => {
            //cy.visit('https://www.saucedemo.com/');
    
            // cy.get('[data-test="username"]').type("locked_out_user");
            // cy.get('[data-test="password"]').type("secret_sauce");
            // cy.get('[data-test="login-button"]').click();
            // cy.get('.title').contains("Products");
            
            //Login através de commands 
            cy.login("locked_out_user","secret_sauce");
            cy.get('[data-test="error"]').contains("Sorry, this user has been locked out.")
            
            //cy.get(elementosLogin.DATATEST_USERNAME_INPUT).type(informacoesLogin.usuario);
            // cy.get(elementosLogin.DATATEST_PASSWORD_INPUT).type(informacoesLogin.senha);
            // cy.get(elementosLogin.DATATEST_LOGIN_BUTTON).click();
           
            // telaLogon.acessarURL();
            // telaLogon.realizarLogin(objeto.loginUsuarioBloqueado);
            // cy.contains(objeto.loginUsuarioBloqueado.mensagem);
        });
    })
   context('Contexto Compra', () =>{
    it('Realizar uma compra completa com mais de um produto no carrinho', () => {
        //     telaLogon.acessarURL();
            //cy.visit('https://www.saucedemo.com/');
        //     telaLogon.realizarLogin(objeto.loginUsuarioStandard);
            cy.get('[data-test="username"]').type("standard_user");
            cy.get('[data-test="password"]').type("secret_sauce");
            cy.get('[data-test="login-button"]').click();
            cy.get('.title').contains("Products");
        //     telaPrincipal.adicionarProdutosCarrinho(objeto.adicionarDoisProdutosCarrinho);
        cy.get('[data-test="add-to-cart-sauce-labs-backpack"]').click();
        //acessa carrinho
        cy.get('.shopping_cart_link').click();
        //valida inventário
        cy.get('.inventory_item_name').contains("Sauce Labs Backpack");
        cy.get('.inventory_item_price').contains("$29.99");
        //checkout
        cy.get('[data-test="checkout"]').click();
        cy.get('[data-test="firstName"]').type("Tony");
        cy.get('[data-test="lastName"]').type("Stark");
        cy.get('[data-test="postalCode"]').type("88007");
        cy.get('[data-test="continue"]').click();
        cy.get('[data-test="finish"]').click();
        //valida compra com sucesso
        cy.get('.title').contains("Checkout: Complete!");
        cy.get('.complete-header').contains("THANK YOU FOR YOUR ORDER");
    
        //     finalizarCompras.acessarCarrinho();
        //     finalizarCompras.informacoesEntrega(objeto.informacoesEntregaDoisProdutos);
        //     cy.contains(objeto.finalizacaoCompraDoisProdutos.valorTotal);
        //     finalizarCompras.finalizarCompra();
        });
   })
     
    // it('Adicionar todos os produtos ao carrinho e validar o valor total da compra a ser pago', () => {
    //     telaLogon.acessarURL();
    //     telaLogon.realizarLogin(objeto.loginUsuarioStandard);
    //     telaPrincipal.adicionarProdutosCarrinho(objeto.adicionarTodosProdutosCarrinho);
    //     finalizarCompras.acessarCarrinho();
    //     finalizarCompras.informacoesEntrega(objeto.informacoesEntregaTodosProdutos);
    //     cy.contains(objeto.finalizacaoCompraTodosProdutos.valorTotal);
    //     finalizarCompras.finalizarCompra();
    // });
});

const objeto = {
    loginUsuarioBloqueado : {
        usuario : 'locked_out_user',
        senha   : 'secret_sauce',
        mensagem: 'Epic sadface: Sorry, this user has been locked out.'
    },
    loginUsuarioStandard : {
        usuario: 'standard_user',
        senha  : 'secret_sauce',
    },
    adicionarDoisProdutosCarrinho : {
        mochila     : true,
        luzBicicleta: true
    },
    informacoesEntregaDoisProdutos : {
        nome        : 'Fulano',
        sobrenome   : 'De tal',
        codigoPostal: '123',
    },
    finalizacaoCompraDoisProdutos : {
        valorTotal: 'Total: $43.18'
    },
    adicionarTodosProdutosCarrinho : {
        mochila      : true,
        luzBicicleta : true,
        blusaBolt    : true,
        jaqueta      : true,
        macacao      : true,
        blusaVermelha: true     
    },
    informacoesEntregaTodosProdutos : {
        nome        : 'Fulano',
        sobrenome   : 'De tal',
        codigoPostal: '123',
    },
    finalizacaoCompraTodosProdutos : {
        valorTotal: 'Total: $140.34'
    }
}

// adicionarProdutosCarrinho(produtos){
//     if (produtos.mochila){
//         cy.get(elementos.DATATEST_BACKPACK_BUTTON).click();
//     }
//     if (produtos.luzBicicleta){
//         cy.get(elementos.DATATEST_BIKE_LIGTH_BUTTON).click();
//     }
//     if (produtos.blusaBolt){
//         cy.get(elementos.DATATEST_BOLT_TSHIRT_BUTTON).click();
//     }
//     if (produtos.jaqueta){
//         cy.get(elementos.DATATEST_JACKET_BUTTON).click();
//     }
//     if (produtos.macacao){
//         cy.get(elementos.DATATEST_ONESIE_BUTTON).click();            
//     }
//     if (produtos.blusaVermelha){
//         cy.get(elementos.DATATEST_RED_TSHIRT_BUTTON).click();
//     }
// }
function acessarURL(){
    cy.visit('https://www.saucedemo.com/');
}

function realizarLogin(informacoesLogin){
    cy.get(elementos.DATATEST_USERNAME_INPUT).type(informacoesLogin.usuario);
    cy.get(elementos.DATATEST_PASSWORD_INPUT).type(informacoesLogin.senha);
    cy.get(elementos.DATATEST_LOGIN_BUTTON).click();
}